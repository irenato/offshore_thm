<?php get_header(); ?>

	<?php if (have_posts()) : ?>

		<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
 	  <?php if (is_day()) { ?>
		<h2 class="pagetitle">Архив за <?php the_time('j F , Y'); ?></h2>
	 	<?php } elseif (is_month()) { ?>
		<h2 class="pagetitle">Архив за <?php the_time('F Y'); ?></h2>
		<?php } elseif (is_year()) { ?>
		<h2 class="pagetitle">Архив за <?php the_time('Y'); ?></h2>
	  <?php } elseif (is_author()) { ?>
		<h2 class="pagetitle">Архив автора</h2>
		<?php } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
		<h2 class="pagetitle">Архив</h2>
		<?php } ?>

		<?php while (have_posts()) : the_post(); ?>
			<div class="post">
				<h3><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
				<div class="postmetadata"><?php the_time('d.m.Y') ?> | Автор: <a href="<?php bloginfo('url'); ?>/author/<?php the_author_login(); ?>/"><?php the_author() ?></a> | Рубрика: <?php the_category(', ') ?> | <?php comments_popup_link('Оставить комментарий', 'Комментариев: 1', 'Комментариев: %'); ?></div>
				<div class="entry">
					<?php the_excerpt(); ?>
				</div>
			</div>

		<?php endwhile; ?>

  	<div class="navigation">
			<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } else { ?>
			<div class="alignleft"><?php next_posts_link('&laquo; Раньше') ?></div>
			<div class="alignright"><?php previous_posts_link('Позже &raquo;') ?></div>
			<?php } ?>
		</div>

	<?php else : ?>

		<h2>Не найдено</h2>
		<p>Извините, по вашему запросу ничего не найдено.</p>

	<?php endif; ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>