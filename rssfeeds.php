<?php
/*
Template Name: RSSfeeds
*/
?>
<?php get_header(); ?>

	<h2><?php the_title(); ?></h2>

	<?php $rssfeeds=true; ?>
 	<?php if (function_exists('article_directory')) article_directory(); ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>