    </div><!-- end #content -->
    
    <div id="sidebars">
      <div id="l-sidebar">

<?php if (!current_user_can('level_0')) { ?>
	      <form name="loginform" id="autoriz" action="<?php echo get_settings('siteurl'); ?>/wp-login.php" method="post">
	        <h2>Авторизация</h2>
	        <label for="login">Логин:</label>
	        <input type="text" name="log" value="" id="login" /><br />
	        <label for="password">Пароль:</label>
	        <input type="password" name="pwd" value="" id="password" />
	        <input type="submit" name="submit" value="Войти" id="enter" />
	        <div class="rememberme"><label for="rememberme"><input name="rememberme" id="rememberme" type="checkbox" value="forever" />Запомнить меня</label></div>
	        <input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
	        <p><?php wp_register('', ''); ?><a href="<?php bloginfo('wpurl'); ?>/wp-login.php?action=lostpassword">Забыли пароль?</a></p>
	      </form>
<?php } else { ?>
	      <div id="manage">
	        <h2>Управление</h2>
	        <ul>
            <li><a href="<?php bloginfo('url'); ?>/wp-admin/post-new.php">Добавить статью</a></li>
<?php if (current_user_can('level_7')) { ?>
            <li><a href="<?php bloginfo('url'); ?>/wp-admin/edit.php">Управление</a></li>
            <li><a href="<?php bloginfo('url'); ?>/wp-admin/edit-comments.php">Комментарии</a></li>
            <li><a href="<?php bloginfo('url'); ?>/wp-admin/plugins.php">Плагины</a></li>
            <li><a href="<?php bloginfo('url'); ?>/wp-admin/users.php">Пользователи</a></li>
            <li><a href="<?php bloginfo('url'); ?>/wp-admin/options-general.php">Настройки</a></li>
<?php } ?>
<?php if (!current_user_can('level_7')) { ?>
            <li><a href="<?php bloginfo('url'); ?>/wp-admin/profile.php">Мой профиль</a></li>
<?php } ?>
<?php if(!function_exists('wp_list_comments')) { ?>
	          <li><a href="<?php echo get_settings('siteurl') . '/wp-login.php?action=logout&amp;redirect_to=' . $_SERVER['REQUEST_URI']; ?>">Выйти</a></li>
<?php } else { ?>
	          <li><a href="<?php echo wp_logout_url($_SERVER['REQUEST_URI']); ?>">Выйти</a></li>
<?php } ?>
	        </ul>
	      </div>
<?php } ?>

<?php	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Сайдбар 1') ) : ?>
	      <h2>Последние статьи</h2>
				<?php
					$myposts = get_posts('numberposts=5');
					foreach($myposts as $post) : setup_postdata($post);
				?>
				<div class="post">
					<h3><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
					<div class="postmetadata"><?php the_time('d.m.Y') ?> @ <?php the_category(', ') ?></div>
					<div class="entry">
					  <?php the_excerpt(); ?>
					</div>
				</div>
				<?php endforeach; ?>
<?php endif; ?>

			</div><!-- end #l-sidebar -->


			<div id="r-sidebar">

<?php	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Сайдбар 2') ) : ?>
				<h2>RSS</h2>
				<ul>
					<li><a href="<?php bloginfo('rss2_url'); ?>">RSS Статей</a></li>
					<li><a href="<?php bloginfo('comments_rss2_url'); ?>">RSS Комментариев</a></li>
				</ul>

				<h2>Архив</h2>
				<ul>
					<?php wp_get_archives('type=monthly'); ?>
				</ul>

				<h2>Ссылки</h2>
				<ul>
					<?php wp_list_bookmarks('categorize=0&title_li='); ?>
				</ul>
<?php endif; ?>

			</div><!-- end #r-sidebar -->
		</div><!-- end #sidebars -->