<?php

add_filter('comments_template', 'legacy_comments');
function legacy_comments($file) {
	if(!function_exists('wp_list_comments')) : // WP 2.7-only check
		$file = TEMPLATEPATH . '/comments-old.php';
	endif;
	return $file;
}



if ( function_exists('register_sidebar') )
    register_sidebar(array(
        'name' => 'Сайдбар 1',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ));

if ( function_exists('register_sidebar') )
    register_sidebar(array(
        'name' => 'Сайдбар 2',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ));
    


function get_wp_vers() {
	$wp_version = file_get_contents(ABSPATH."wp-includes/version.php");
	preg_match("/'(.*)'/is", $wp_version, $out);
	$out = $out[1];
	preg_match("/\d\.\d/i", $out, $match);
	return $match[0];
}



/* Количество постов */
if (get_wp_vers() < '2.3') $ptype = '';
	else $ptype = " AND post_type = 'post'";

$numposts = $wpdb->get_var("SELECT COUNT(*) FROM ".$wpdb->posts." WHERE post_status = 'publish'".$ptype);
if (0 < $numposts) $numposts = number_format($numposts);
$numposts = preg_replace("','", '', $numposts);

/* Количество авторов */
$users = $wpdb->get_var("SELECT COUNT(*) FROM ".$wpdb->users);
if (0 < $users) $users = number_format($users);
$users = preg_replace("','", '', $users);



function declension($int, $expressions)
{
    settype($int, "integer");
    $count = $int % 100;
    if ($count >= 5 && $count <= 20) {
        $result = "<strong>".$int."</strong> ".$expressions['2'];
    } else {
        $count = $count % 10;
        if ($count == 1) {
            $result = " <strong>".$int."</strong> ".$expressions['0'];
        } elseif ($count >= 2 && $count <= 4) {
            $result = " <strong>".$int."</strong> ".$expressions['1'];
        } else {
            $result = " <strong>".$int."</strong> ".$expressions['2'];
        }
    }
    return $result;
}



function declens($int, $expressions)
{
    settype($int, "integer");
    $count = $int % 100;
    if ($count >= 5 && $count <= 20) {
        $result = "зарегистрировано <strong>".$int."</strong> ".$expressions['2'];
    } else {
        $count = $count % 10;
        if ($count == 1) {
            $result = "зарегистрирован <strong>".$int."</strong> ".$expressions['0'];
        } elseif ($count >= 2 && $count <= 4) {
            $result = "зарегистрировано <strong>".$int."</strong> ".$expressions['1'];
        } else {
            $result = "зарегистрировано <strong>".$int."</strong> ".$expressions['2'];
        }
    }
    return $result;
}

?>