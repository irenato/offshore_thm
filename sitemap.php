<?php
/*
Template Name: Sitemap
*/
?>
<?php get_header(); ?>

			<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>

        <div class="post">
			      <h2><?php the_title(); ?></h2>

				    <div class="entry">
			        <?php the_content(); ?>
              <?php blix_archive(false, '<h3>', '</h3>', 'sitemap'); ?>
				    </div>

				    <?php edit_post_link('редактировать','<p>','</p>'); ?>

        </div>

			<?php endwhile; ?>

			<?php else : ?>

			  <h2>Не найдено</h2>
			  <p>Извините, по вашему запросу ничего не найдено.</p>

			<?php endif; ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>