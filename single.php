<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<div class="post">
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
				<div class="postmetadata">Опубликовано: <?php the_time('d.m.Y') ?> | Автор: <?php the_author_posts_link(); ?> | Рубрика: <?php the_category(', ') ?></div>



				<div class="entry">



				  <?php the_content('Далее &raquo;'); ?>
				</div>



				<?php edit_post_link('Редактировать', '<p>', '</p>'); ?>
			</div>

	<?php comments_template(); ?>

	<?php endwhile; else: ?>

		<h2>Не найдено</h2>
		<p>Извините, по вашему запросу ничего не найдено.</p>
		<?php include (TEMPLATEPATH . "/searchform.php"); ?>

<?php endif; ?>

<div class="other">
	<h3>Другие статьи рубрики "<?php $cat = get_the_category(); $cat = $cat[0]; echo $cat->cat_name; ?>"</h3>
	<ul class="recent">
		<?php
		$myposts = get_posts('numberposts=10&offset=0&exclude='.$id.'&category='.$cat->cat_ID);
		foreach($myposts as $post) :
		?>
		<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
	<?php endforeach; ?>
	</ul>
</div>


<?php get_sidebar(); ?>
<?php get_footer(); ?>